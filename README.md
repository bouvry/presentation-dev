# Présentation DEV

Support de cours

## Cours / Présentation

 - [2020 Novembre - VueJS 2.0](src/vuejs.md)
 
## Votre propre présentation

Les supports sont utilisés pour générer des diapositives de présentation via **revealjs** (https://revealjs.com/) à partir de fichier **Markdown** avec l'utilitaire **Pandoc** (https://pandoc.org/).

Pour **créer vos propres présentation**, clonez le dépôt : 

```bash
git clone https://git.unicaen.fr/bouvry/presentation-dev.git
cd presentation-dev
```

Installez **Pandoc**
```bash
apt get install pandoc
```

Installez les **dépendances NodeJS**
```bash
npm install
```

Créer un dossier pour votre présentation

```bash
mkdir src/exemple
```

Puis ajoutez un fichier markdown structuré de cette façon : 

```markdown
% TITRE PRÉSENTATION
% Dev Note
% Novembre 2020

# Partie 1

## Titre slide 1.1

Contenu

## Titre 2.2

Contenu


# Partie 2

## Slide 2.1

Etc...
```

Puis compilez : 

```bash
pandoc -t revealjs -s src/exemple/votre-fichier.md -o dist/vuejs.html
```



