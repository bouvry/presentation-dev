const { watch, src, dest } = require('gulp');
const { exec } = require("child_process");

const pathSrc = 'src/';
const pathDest = 'dist/';

var options = {
    continueOnError: false, // default = false, true means don't emit error event
    pipeStdout: false, // default = false, true means stdout is written to file.contents
};
var reportOptions = {
    err: true, // default = true, false means don't write err
    stderr: true, // default = true, false means don't write stderr
    stdout: true // default = true, false means don't write stdout
};

function buildVueJs(cb){
    exec("mkdir -p dist && cp -r libs/* dist && pandoc -t revealjs -s src/vuejs/*.md -o dist/vuejs.html", (error, stdout, stderr) => {
        if( error ){
            console.error(`ERROR : ${error.message}`);
        }
        if( error ){
            console.error(`STDERR : ${stderr}`);
        }
        console.log(`${stdout}`);
    });
    return cb();
}

function clean(cb){
    exec("rm -Rf dist", (error, stdout, stderr) => {
        if( error ){
            console.error(`ERROR : ${error.message}`);
        }
        if( error ){
            console.error(`STDERR : ${stderr}`);
        }
        console.log(`${stdout}`);
    });
    return cb();
}

exports.default = function() {
    watch('src/**/*.md', buildVueJs);
};

exports.buildVueJs = buildVueJs;
exports.clean = clean;