# Premiers pas

## Utilisation simple

Prise en main et découverte **de base** (pour tester rapidement) :

## Hello World

```html
<script src="../lib/vue.js"></script>
<div id="app"></div>
<script type="text/javascript">
new Vue({
  el: "#app",
  template: `<h1>Hello World</h1>`
});
</script>
```

Le template est dans **l'instance de Vue**

---

Idem, mais avec le template dans le DOM :

```html
<script src="../lib/vue.js"></script>
<div id="app">
  <h1>Hello World</h1>
</div>
<script type="text/javascript">
new Vue({
  el: "#app"
});
</script>
```

---

Dans les deux cas, ça fonctionne pour des vues simples...

---

## Data binding

Vue fonctionne avec un modèle **data** qui sera automatiquement **réactif** :
```html
<div id="app">
  <h1>Hello {{ who }} !!!</h1>
</div>
```
```js
var myApp = new Vue({
  el: "#app",
  data: {
    who: "All The World"
  }
});
```

Testez dans la **console** : `myApp.who = "Codeur"`

---

Pour utiliser une donnée dans un attribut, on utilise la **directive** `v-bind:attribute` :

```html
<div id="app">
  <h1 v-bind:title="who">Survolle moi</h1>
</div>
```

```js
new Vue({
  el: "#app",
  data: {
    who: "All The World"
  }
});
```
