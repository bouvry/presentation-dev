# Sous le capot

## render

Le rendu des vues repose sur la méthode `render` :

```html
<div id="app"></div>
```

```js
new Vue({
  el: "#app",
  data: {
    msg: "Hello World"
  },
  render(createElement){
    return createElement('h1', this.msg);
  }
});
```

---

Le rendu est basé une un *Virtual DOM* : Optimisation, rapidité

```js
var myApp = new Vue({
  el: "#app",
  data(){
    return {
      who: "Foo"
    }
  },
  render(createElement){
    return createElement('p',
      [ 'Hello ', createElement('strong', this.who), " !" ]
    );
  }
});
```

---

Les *templates* sont convertis en fonction `render(h)`, pour info, la classe **Vue** propose une méthode **compile** pour produire cette compilation :

```js
var template = "<h1>{{ variable }}</h1>";
console.log(Vue.compile(template));
```

---

En production / situation *rééle*, on utilise des fichiers **.vue** associés à des utilitaires **nodejs** pour obtenir du javascript compilé.
