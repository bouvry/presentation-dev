# Datas et méthodes

## data

Chaque instance dispose d'un modèle interne et *réactif* via la propriété `data` : 

```js
var myApp = new Vue({
  data(){
    return {
      foo: "Valeur FOO",
      bar: "Valeur BAR",
      liste: ["item A", "item B", "item C"]
    }
  }
});
```

---

La clef `data` se présente souvent sous la forme d'une méthode retournant un objet de donnée pour éviter les problèmes de pointeur (plusieurs instances) : 

```js
var myApp = new Vue({
  data(){
    return {
      /* Donnèes */
    }
  }
});
```

---

Les propriétés *doivent être initialisées* dans l'objet `data` pour garantir leur *réactivité*.

## methods

La clef `methods` permet d'ajouter des logiques personnalisées (`this` fait référence à l'instance).

```js
var myApp = new Vue({
  data(){
    return {
        valeur: "initiale"
    }
  },
  methods: {
    handlerRandom(){
      this.valeur = "Nouvelle valeur " + Math.random()
    }
  }
});
```