# Template

## Rendu textuel

Utilise la **syntaxe moustache** : 

```html
<div>
    <h1>{{ valeur }}</h1>
</div>
```

## Expression JS

Le contenu d'une *moustache* est une expression Javascript : 

```html
<div>
    <h1>Partie {{ nombre + 5 }}</h1>
    <p>Il y'a {{ totalPersonne }} 
        connectée{{ totalPersonne > 1 ? 's' : '' }}</p>
</div>
```

## Attributs dynamique

Utilise la directive `v-bind:attribut` ou `:attribut` : 

```html
<div>
    <h1 :title="valeur">{{ valeur }}</h1>
</div>
```

---

le contenu de l'attribut est une **expression Javascript** : 

```html
<div>
    <h1 :title="'Valeur vaut : ' + valeur">{{ valeur }}</h1>
</div>
```

## v-for : Array

Affichage des données type *forable* (Tableau, Object);

```js
var myApp = new Vue({
  data(){
    return {
        lettres: ["A","B","C"]
    }
  }
});
```

```html
<ul>
    <li v-for="lettre in lettres">{{ lettre }}</li>
</ul>
```

---

Liste d'objet :

```js
var myApp = new Vue({
  data(){
    return {
      languages: [
        { label: "Javascript" },
        { label: "PHP" },
        { label: "CSS" }
      ]
    }
  }
});
```

```html
<ul>
    <li v-for="l in languages">{{ l.label }}</li>
</ul>
```

## v-for : Object

Affichage des données type *forable* (Tableau, Object);

```js
var myApp = new Vue({
  data(){
    return {
      informations: {
        title: "Titre",
        description: "Description"
      }
    }
  }
});
```

```html
<ul>
    <li v-for="valeur in information">{{ valeur }}</li>
</ul>
```

## v-for : Clef

Affichage des clef : 

```js
var myApp = new Vue({
  data(){
    return {
      informations: {
        title: "Titre",
        description: "Description"
      }
    }
  }
});
```

```html
<ul>
    <li v-for="valeur,clef in informations">{{ clef }} : {{ valeur }}</li>
</ul>
```

## Conditions

Les **directives** `v-if="condition"`, `v-elseif="condition"` et `v-else` 

```html
<div v-if="message">
    <p>{{ message }}</p>
</div>
```

---

```html
<div v-if="error">
    <p>Erreur : {{ error }}</p>
</div>
<div v-else>
    <p>Tout fonctionne bien</p>
</div>
```

## Le cas V-SHOW

Même principe que pour `v-if`, mais utilise CSS pour cacher l'élément (pas de modification du DOM) : 

```html
<div v-show="message">
    <p>{{ message }}</p>
</div>
```
