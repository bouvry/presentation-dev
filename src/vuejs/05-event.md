# Events

## DOM

On peut capter les événements natif du DOM avec la directive `v-on:EVENT` ou `@EVENT` :

```html
<button @click="nbrClick++">
    Clique : {{ nbrClick }}
</button>
```

## Event et méthodes simples

```html
<button @click="handlerClick">Demo</button>
```

```js
new Vue({
    // ...
    methods: {
        handlerClick(e){
            console.log(e, "déclenché !");
            alert("Click !")
        }   
    }
})
```


## Modifiers

Vue propose une liste de *modifiers* usuels.

```html
<a @click.prevent="nbrClick++" href="#">
    Clique : {{ nbrClick }}
</a>
```

---

 - `prevent` : Ajoute un `event.preventDefault`
 - `stop` : Ajoute un `event.stop`
 - `capture` : Capture les événements de passage
 - `self` : Ne capture que les événements de l'élément
 - `once` : n'autorise d'une fois l'événement
 
-- 

Les modifiers sont très utilisés pour la capture des événements claviers : 

```html
<input :keyup.enter="handlerSubmit" />
```

Voir la doc pour plus d'informations