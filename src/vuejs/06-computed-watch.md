# Propriétés calculées et surveillance

## Propriétés calculées : computed

```html
<h1> Casting : {{ totalArtistes }} artistes
<button @click="acteurs.push('José')">Ajouter</button>
```

```js
var v = new Vue({
  el: '#app',
  data: {
    acteurs: ["Nicolas", "Etienne", "Cricri"],
    actrices: ["Hélene", "Cathy", "Johanna", "Bénédicte"]
  },
  computed: {
    // a computed getter
    totalArtistes: function () {
      // `this` points to the vm instance
      return this.acteurs.length + 
            this.actrices.length;
    }
  }
})
```

---

Le recalcule des propriétés calculées est dépendants des données réactive. 

**Attention** : Les méthodes semblent donner le même résultat, mais elles sont réexécutées à chanque rendu.

## Surveillance : Watchers

Les **watchers** permettent d'ajouter des comportements lors de changement dans le modèle : 

```js
var v = new Vue({
  el: '#app',
  data: {
    valeur1: "foo",
    valeur2: "bar",
    nombreChangement: 0
  },
  watch: {
    valeur1: function ( val ) {
      console.log('Valeur 1 a pris pour valeur : ' + val);
      this.nombreChangement++;
    },
    valeur2: function ( val ) {
      console.log('Valeur 2 a pris pour valeur : ' + val);
      this.nombreChangement++;
    }
  }
})
```

## Getter / Setter

On peut également surcharger les affectations : 

```js
var v = new Vue({
  el: '#app',
  data: {
    firstName: "",
    lastName: ""
  },
  computed: {
    fullName: {
      // getter
      get: function () {
        return this.firstName + ' ' + this.lastName
      },
      // setter
      set: function (newValue) {
        var names = newValue.split(' ')
        this.firstName = names[0]
        this.lastName = names[names.length - 1]
      }
    }
  }
})
```

