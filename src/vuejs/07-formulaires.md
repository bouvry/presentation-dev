# Formulaires et V-MODEL

## input

La directive `v-model` concerne les éléments de formulaire et automatise le *binding* entre d'UI et le modèle : 

```html
<input type="text' v-model="fullname" />
<p>Fullname : {{ fullname }}</p>
```

## textarea

On est **obligé** d'utiliser `v-model` : 
```html
<textarea type="text' v-model="comment"></textarea>
<p>Commentaire : {{ comment }}</p>
```

## checkbox

Fonctionne avec un booleen : 
```html
<div :class="accepted ? 'accept' : 'notaccept'">
    Accepter les conditions ?
    <input type="checkbox" v-model="checked" />
</div>
```

```js
new Vue({
    // ...
    data(){
        return {
            accepted: false
        }
    }
})
```

---

ou une liste : 

```html
Présents : 
<label for="jack">JACK</label>
<input id="jack" type="checkbox" value="jack" v-model="presents" />
<label for="serge">SERGE</label>
<input id="serge" type="checkbox" value="serge" v-model="presents" />
<code>{{ presents }}</code>
```

```js
new Vue({
    // ...
    data(){
        return {
            presents: ["serge"]
        }
    }
})
```

## Radio

```html
Choississez le coupable : 
<label for="jack">JACK</label>
<input id="jack" type="radio" value="jack" v-model="coupable" />
<label for="serge">SERGE</label>
<input id="serge" type="radio" value="serge" v-model="coupable" />
<code>{{ coupable }}</code>
```

```js
new Vue({
    // ...
    data(){
        return {
            coupable: ""
        }
    }
})
```

## Select

```html
<select v-model="coupable">
    <option disabled value="">Choissisez le coupable</option>
    <option value="jack">JACK</option>
    <option value="serge">SERGE</option>
</select>
<code>Coupable : {{ coupable }}</code>
```

---

Voir la doc pour des utilisations avancées

