# Classes CSS

## Via des objets

Sous la forme : 

```html
<section>
    <div :class="{ 'ClasseCSS': CONDITION }">
        S'affiche si CONDITION est TRUE
    </div>
</section>
```

## Computed

```html
<div :class="stylesCss">
    Exemple
</div>
```

```js
new Vue({
    // ...
    computed: {
        stylesCss(){
            return {
                'active': this.isActive,
                'has-error': this.errors.length > 0
            }       
        }   
    }
})
```

## Style

```html
<div v-:style="{ color: activeColor, fontSize: fontSize + 'px' }">
    Cracra mais fonctionne
</div>
```
