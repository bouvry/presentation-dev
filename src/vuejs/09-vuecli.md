# Vue CLI

## But

 - Structurer son code en plusieurs fichier (Composants, plusieurs applications)
 - Simplifier le debug
 - Exporter pour la prod (Minification, compatibilié, tutti quanti)
 - Prototyper rapidement
 

## Installation

**VueCLI** est un utilitaire **NodeJS** (*Node 8.9+*, *Node 10* recommandé). Si besoin, vous pouvez utiliser **NVM** pour gérer et faire cohabiter plusieurs versions différentes de **Node**/**Npm** sur votre système.

```bash
# Installation "locale"
cd path/to/project/root
npm install --save-dev @vue/cli
npm install --save-dev @vue/cli-service-global
```

```bash
# Installation "globale"
npm install -g @vue/cli
npm install -g @vue/cli-service-global
```

## Fichier Vue

**VueCli** va permettre d'oragniser votre code dans un fichier `*.vue`.

Ce fichier est divisé en 3 sections : 

 - `template`
 - `script`
 - `style` (optionnel) 

--- 

Le fichier se présente ainsi : 

```html
<!-- AppSimple.vue -->
<template>
    <h1>Mon Composant : {{ message }}</h1>
    <input type="text" v-model="message" />
</template>

<script>
    export default {
        data(){
            return {}
        }
    }
</script>

<style>
    h1 { color: red }
</style>
```

## Prototype, 0 configuration

Vous pouvez *servir* directement ensuite avec utiliser la commande `vue`, ou `node node_modules/.bin/vue` sur un fichier *.vue* ou un fichier *.js* pour déployer un serveur de développement pour travailler sur la vue/composant : 

```bash
node node_modules/.bin/vue serve MonFichier.vue
```

Le fichier est servi via un serveur HTTP local (sur le port 8080 par défaut) intégrant du rechargement en temps réél et une remontée d'erreur plus claire.


## Gabarit HTML

Vous pouvez personnaliser l'intégration HTML en créant un dossier **public** puis un fichier `index.html` : 

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>Ma Super Application</title>
    </head>
<body>
    <div id="app"></div>
</body>
</html>
```

> Le injection des balises `script` est automatique.


## CSS statique/images

Le dossier `public` contient les fichiers *statiques*,  (librairies), Images, etc... L'accès à ces fichiers peut être définit depuis le gabarit HTML en utilisant la viariable de gabarit `BASE_URL`

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <!-- etc -->
        <link rel="icon" href="<%= BASE_URL %>favicon.ico">
       
```

## Mise en production (Build)

Basé sur **Webpack**, la commande `build` permet de compiler (Adapter de minifier) les sources (fichier VUE/JS/CSS) dans un (ou plusieurs fichiers).

```bash
node node_modules/.bin/vue build MonFichier.vue
```

## Configuration pratique

Le fichier `config.vue.js` permet de configurer différents paramètres. Cette configuration est très détaillées dans la documentation : [https://cli.vuejs.org/config/]()

## publicPath

`publicPath` qui permet de modifier le chemin *root* pour le build : 

```js
// config.vue.js
module.exports = {
    // base path du build
    publicPath: "./",
}
```

## Output

`outputDir` permet de modifier le nom du dossier de sortie pour le build : 
   
```js
// config.vue.js
module.exports = {
   // Dossier de sortie
       outputDir: "dist"
}
``` 

