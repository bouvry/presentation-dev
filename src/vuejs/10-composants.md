# Composants

## Déclarer un composant

Méthode classique : 

```js
// Composant.js
var Compteur = {
    template: `<button @click="count++">COUNT {{ count }}</button>`,
    data(){ 
        return {
            count: 0
        }
    }
};
```

---

Avec un fichier Vue : 

```html
<!-- Composant.vue -->
<template>
    <button @click="count++">COUNT {{ count }}</button>
</template>
<script>
    export default {
       data(){ 
           return {
               count: 0
           }
       } 
    }
</script>
```

Techniquement, la déclaration d'un composant ou d'une vue est identique. Avec VueCLI, les fichiers *.vue sont des composants :P

## Importer

Méthode traditionnelle (Vieux JS), avec script

```html
<script src="Composant.js"></script>
```

Avec du JS récent (Ou si vous utilisez Babel) ou VUECLI (Qui utilise Babel en vrai)

```js
import Compteur from './Compteur'
```

## Référencer un composant

On peux faire un référencement **global**, le composant sera alors disponible dans toutes les vues/composants.

```js
// Pour un usage "global", on référence le composant
Vue.component('compteur', Compteur);
```

---

On peut aussi déclarer un composant localement (Dans une Vue ou un composant) : 

```js
export default {
    components: {
        'compteur': Compteur
    }
}
```

## Afficher un composant

Dans le **template**, on utilise ensuite le nom choisi lors du référencement pour l'utiliser : 

```html
<div>
    <compteur></compteur>
</div>
```

---

On peut utiliser plusieurs fois un composant : 

```html
<div>
    <compteur></compteur>
    <compteur></compteur>
    <compteur></compteur>
</div>
```

## Data et composant

Dans un composant, la clef `data` **DOIT** être une fonction qui retourne un objet pour éviter les problèmes de pointeur !

```js
export default {
   //////////////////////// DATA !
   data(){ 
       return {
           count: 0
       }
   }
   ///////////////////////////////
}
```

## Props

les **props** permettent de transmettre des donnèes aux composants : 

```js
export default {
    // On identifie un propriété 'value'
    props: [ 'value' ]
    // ...
}
```

```html
<div>
    <compteur value="5"></compteur>
    <compteur value="8"></compteur>
    <compteur value="13"></compteur>
</div>
```

> On peut maintenir la réactivité avec la directive `v-bind:value`

## Props réactives

Exemple, une application avec 3 titres dans le modèle : 

```js
import Titre from './Titre';
export default {
    components: {
        "titre": Titre
    },   
    data(){
        return {
            titre1: "Foo",
            titre2: "Bar",
            titre3: "Foo-Bar",
        }
    }
}
```
```html
<div>
    <titre :value="titre1"></titre>
    <titre :value="titre2"></titre>
    <titre :value="titre3"></titre>
</div>
```

--- 

On ne peut/doit pas modifier la source des données depuis le composant, il faudra utiliser des événements !

```html
<!-- Composant.vue -->
<!-- NE FONCTIONNE PAS !!! -->
<template>
    <h3 @click="value = 'default'">{{ value }}</h3>
</template>
<script>
    export default {
        props: ['value']
    }
</script>
```

## Events : $emit

Vous pouvez utiliser `$emit` pour émettre des événements. `$emit(EVENT, param1, paramN...)` permet d'émettre un événement de type EVENT avec autant de paramètres que necessaire.

```html
<!-- Composant.vue -->
<template>
    <div>
        <h3 @click="$emit('reset', 'default')">{{ value }}</h3>
        <button @click="setToto">Mettre la valeur toto</button>
    </div>
</template>

<script>
    export default {
        props: ['value'],
        methods:{
            setToto(){
                this.$emit('reset', 'toto');
            }
        }   
    }
</script>
```

---

Ensuite, on capte l'évenement émit depuis les composants comme pour un événement classique : 

```html
<!-- App.vue -->
<div>
    <titre :value="titre1" @reset="titre1 = $event"></titre>
    <titre :value="titre2" @reset="titre2 = $event"></titre>
    <titre :value="titre3" @reset="titre3 = $event"></titre>
    <input type="text" v-model="titre1" />
    <input type="text" v-model="titre2" />
    <input type="text" v-model="titre3" />
</div>
```

Pas très élégant

--- 

La reception et le traitement d'un événement passe plutôt par une méthode : 

```html
<div>
    <titre :value="titre1" @reset="handlerReset1"></titre>
    <!-- etc... -->
    <input type="text" v-model="titre1" />
    <!-- etc... -->
</div>
```

```js
export default {
    // ...
    methods: {
        handlerReset1(valeur){
            console.log("Reçu", arguments);
            this.titre1 = valeur;
        }   
    }   
}
```

-- 

On peut envoyer plusieurs données avec `$emit` : 


```html
<!-- Composant.vue -->
<template>
    <div>
        <h3 @click="$emit('reset', 'foo', 'bar')">{{ value }}</h3>
        <button @click="setToto">Mettre la valeur toto</button>
    </div>
</template>

<script>
    export default {
        props: ['value'],
        methods:{
            setToto(){
                this.$emit('reset', 'toto', 'tata');
            }
        }   
    }
</script>
```

---

il y'aura plusieurs paramètres dans la fonction handler : 

```js
handlerResetA(valeur1, valeur2){
    console.log("Reçu", arguments);
    this.titre1 = valeur1  +' ' +valeur2;
}
```



