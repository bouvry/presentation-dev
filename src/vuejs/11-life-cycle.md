
# Lifesycle Hooks

## created

Lorsque l'instance est créée (mais pas encore présente dans le DOM).

```js
new Vue({ 
    /* code */
    created(){
        console.log('Instance créée');
    }
});
```

## mounted

Lorque l'instance est **montée** dans le DOM.

```js
new Vue({ 
    /* code */
    mounted(){
        console.log('Instance créée');
    }
});
```

## Lifecycle

Pour d'information sur la documentation officielle : https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram

